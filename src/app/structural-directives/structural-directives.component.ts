import { Component } from '@angular/core';

@Component({
  selector: 'app-structural-directives',
  templateUrl: './structural-directives.component.html',
  styleUrl: './structural-directives.component.css'
})
export class StructuralDirectivesComponent {

  person = { name: "andre", age: 41 };
  names = ['andre', 'abdelfattah', 'thierry', 'julien', 'christophe', 'guillaume', 'akrem'];

}
