import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrl: './reactive-form.component.css'
})
export class ReactiveFormComponent {

  personForm = new FormGroup({
    name: new FormControl('andre', [Validators.required, Validators.minLength(3)]),
    age: new FormControl(41, [Validators.min(0), Validators.max(150)])
  });

  onSubmit() {
    console.log(this.personForm);
  }

}
