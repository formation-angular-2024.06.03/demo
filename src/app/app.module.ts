import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Page1Component } from './routing/page1/page1.component';
import { Page2Component } from './routing/page2/page2.component';
import { HomeComponent } from './routing/home/home.component';
import { ParentComponent } from './component-interactions/parent/parent.component';
import { ChildComponent } from './component-interactions/child/child.component';
import { TemplateDrivenFormComponent } from './template-driven-form/template-driven-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { HtmlTsInteractionsComponent } from './html-ts-interactions/html-ts-interactions.component';
import { StructuralDirectivesComponent } from './structural-directives/structural-directives.component';
import { RoutingComponent } from './routing/routing.component';
import { ComponentInteractionsComponent } from './component-interactions/component-interactions.component';

@NgModule({
  declarations: [
    AppComponent,
    Page1Component,
    Page2Component,
    HomeComponent,
    ParentComponent,
    ChildComponent,
    TemplateDrivenFormComponent,
    ReactiveFormComponent,
    HtmlTsInteractionsComponent,
    StructuralDirectivesComponent,
    RoutingComponent,
    ComponentInteractionsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
