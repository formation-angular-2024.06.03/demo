import { Component } from '@angular/core';

@Component({
  selector: 'app-html-ts-interactions',
  templateUrl: './html-ts-interactions.component.html',
  styleUrl: './html-ts-interactions.component.css'
})
export class HtmlTsInteractionsComponent {

  person = { name: "andre", age: 41 };
  link = 'http://www.goggle.com';

  incrementAge() {
    this.person.age++;
  }
}
