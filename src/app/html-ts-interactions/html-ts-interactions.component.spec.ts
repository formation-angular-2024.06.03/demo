import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlTsInteractionsComponent } from './html-ts-interactions.component';

describe('HtmlTsInteractionsComponent', () => {
  let component: HtmlTsInteractionsComponent;
  let fixture: ComponentFixture<HtmlTsInteractionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HtmlTsInteractionsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(HtmlTsInteractionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
