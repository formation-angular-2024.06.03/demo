import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrl: './child.component.css'
})
export class ChildComponent {
  @Input() name?: string;
  @Output() increment: EventEmitter<number> = new EventEmitter<number>();

  onIncrementClicked() {
    this.increment.emit(this.name?.length);
  }
}
