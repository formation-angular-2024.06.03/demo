import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrl: './page2.component.css'
})
export class Page2Component implements OnInit, OnDestroy {

  nameFromSnapshot?: string;
  nameFromObservable?: string;

  private subscription?: Subscription;

  constructor(private activatedRoute: ActivatedRoute) {
    this.nameFromSnapshot = activatedRoute.snapshot.params['name'];
  }

  ngOnInit(): void {
    this.subscription = this.activatedRoute.params.subscribe(params => this.nameFromObservable = params['name']);
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }
}
