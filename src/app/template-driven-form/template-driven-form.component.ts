import { Component } from '@angular/core';

type Person = {
  name: string;
  age: number;
}

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrl: './template-driven-form.component.css'
})
export class TemplateDrivenFormComponent {

  person: Person = { name: 'andre', age: 41 };

  onSubmit() {
    console.log(this.person);
  }
}
